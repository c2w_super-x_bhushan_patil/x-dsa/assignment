import java.util.*;
class UpperLower {

	public static void main (String b[]) {
		
	Scanner sc = new Scanner(System.in);	
		String str = sc.next();

		char arr [] = str.toCharArray();

		for (int i = 0; i < arr.length; i++) {
		
			if(i%2 == 0) {
				
				if(arr[i] > 97 && arr[i] < 122){
					arr[i] -= 32;
				}
			}else{
				
				if(arr[i] > 65 && arr[i] < 90){
					arr[i] += 32;
				}
			}
		}

		for (int x : arr) {
			
			System.out.print((char)x);
		}
		System.out.println();
	}
}
