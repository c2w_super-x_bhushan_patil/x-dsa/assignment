//256946 = 2,720,24,720;

import java.util.*;
class EvenFactorial {

        static int reverse (int n) {
 		int rev = 0;
 		while (n != 0) {
                        int rem = n % 10;
                        rev = rev * 10 + rem;
			n = n / 10;
		}
                return rev;
        }

        static int factorial (int n) {
		int sum = 1;
		for (int i = 1; i <= n; i++) {
			sum = sum * i;		
		}
		return sum;
	}

        static void Evenfactorial (int n) {
                int temp = reverse(n);
		int sum = 0;
		
		while (temp != 0) {
			int rem = temp % 10;
			if (rem%2 == 0) 
			System.out.println(factorial(rem));
			
			temp = temp / 10;
		}
			
        }
	
        public static void main (String b []) {

		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();

		 Evenfactorial(n);
			
		
        }
}
