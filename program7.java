import java.util.*;
class EvenOdd {

	public static void main (String b[]) {
		
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		int arr1 [] = new int [n];

		for (int i = 0; i < arr1.length; i++) {
			arr1[i] = sc.nextInt();
                }

		int arr2 [] = new int [arr1.length];

		for (int i = 0; i < arr1.length; i++) {
			
			if (arr1[i]%2 == 0)
				arr2[i] = 1;
			else
				arr2[i] = 0;
		}

		for (int x : arr2) {
		
			System.out.print(x+" ");
		}

		 System.out.println();
	}
}
