import java.util.*;
class Palindrome {
	
	public static void main (String b[]) {
		
		Scanner sc = new Scanner(System.in);	
		String str = sc.next();

		char arr [] = str.toCharArray();

		int i = 0;
		int j = arr.length-1;
		int flag = 0;

			while (i < j) {
				
				if (arr[i] != arr[j]) {
					System.out.println(str+" is not palindrome.");
					break;	
				}else{
					flag = 1;
				}

				i++;
				j--;
			}

			if (flag == 1)
				System.out.println(str+" is palindrome.");
	}
}
