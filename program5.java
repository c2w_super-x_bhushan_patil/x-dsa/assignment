import java.util.*;

class ReverseSum {

	public static void main (String b[]) {
		
	  Scanner sc = new Scanner (System.in);

		int n = sc.nextInt();
		int temp = n;
		int count = 0;

		while (n!=0) {
			count++;
			n = n / 10;
		}

		int x = temp%10;
                temp = temp/10;

		for (int i = 1; i <= count; i++) {
			
			int y = temp%10;
                        temp = temp/10;

			System.out.print(x+y+" ");

			x = y;


		}
		System.out.println();
	}
}
