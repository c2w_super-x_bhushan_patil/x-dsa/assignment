import java.util.*;
class Armstrong {

	static int countd (int n) {
		int count = 0;		
		while (n != 0) {
			count++;
			n = n / 10;
		}

		return count;
	}

	static void armstrong (int n) {
		
		int temp = n;
		int c = countd(n);
		int sum = 0;

		while (n != 0) {

			int pow = 1;
			int rem = n % 10;
		
			for (int i = 1; i <= c; i++) {
				
				pow = pow * rem;
			}

			sum = sum + pow;
			n = n / 10;
		}

		if (sum == temp) {
			System.out.println(temp+ " is armstrong number.");
		}else{
			System.out.println(temp+ " is not armstrong number.");
		}
	}
	
	public static void main (String b []) {
	
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();

		armstrong(n);
	}
}
