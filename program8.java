import java.util.*;
class Vowels {
	
	public static void main (String b[]) {

		Scanner sc = new Scanner(System.in);

		String str = sc.next();

		char arr [] = str.toCharArray();

		int ac = 0;
	       	int ec = 0;
		int ic = 0;
		int oc = 0;
		int uc = 0;

		for (int i = 0; i < arr.length; i++) {
			
			if(arr[i] == 'a' || arr[i] == 'A')
				ac++;
			else if(arr[i] == 'e' || arr[i] == 'E') 
				ec++;	
			else if(arr[i] == 'i' || arr[i] == 'I') 
				ic++;
			else if(arr[i] == 'o' || arr[i] == 'O') 
				oc++;
			else if(arr[i] == 'u' || arr[i] == 'U') 
				uc++;
		}

		System.out.println("a = "+ac);
		System.out.println("e = "+ec);
		System.out.println("i = "+ic);
		System.out.println("o = "+oc);
		System.out.println("u = "+uc);
	}
}
