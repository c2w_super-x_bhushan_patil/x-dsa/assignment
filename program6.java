import java.util.*;
class LeftRight {
	
	public static void main (String b []) {

	Scanner sc = new Scanner(System.in);	
		int n = sc.nextInt();
		int temp = n;
		int count = 0;

		while (n != 0) {
			count++;
			n = n / 10;
		}

		int arr [] = new int[count];
		
		for (int i = 0; i < arr.length; i++) {
                        arr[i] = temp % 10;
                	temp = temp / 10;
                }

		for (int i = 1; i < arr.length-1; i++) {
				
			if(arr[i+1] < arr[i] && arr[i-1] < arr[i]) {
				System.out.println(arr[i]);
				break;
			}
		}

	}
}
